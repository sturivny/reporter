from unittest import TestCase
from unittest.mock import MagicMock

from reporter.utils import join_addresses
from reporter.utils import status_to_emoji
from reporter.utils import unique_test_descriptions
from reporter.utils import yesno


class TestUtils(TestCase):
    """Tests for reporter/utils.py ."""

    def test_yesno(self):
        """Test yesno filter/function."""
        data = [
            [{'condition': True, 'values': 'yeah,no,maybe'}, 'yeah'],
            [{'condition': False, 'values': 'yeah,no,maybe'}, 'no'],
            [{'condition': None, 'values': 'yeah,no,maybe'}, 'maybe'],

            [{'condition': True, 'values': ' well done ,no,maybe'},
             ' well done '],
            [{'condition': False, 'values': 'yes, try again ,maybe'},
             ' try again '],
            [{'condition': None, 'values': 'yes,no, who knows '},
             ' who knows '],

            [{'condition': True, 'values': 'yes,no'}, 'yes'],
            [{'condition': False, 'values': 'yes,no'}, 'no'],
            [{'condition': None, 'values': 'yes,no'}, 'no'],

            [{'condition': True}, 'yes'],
            [{'condition': False}, 'no'],
            [{'condition': None}, 'no']
        ]
        for args, result in data:
            self.assertEqual(yesno(**args), result)
        self.assertRaises(IndexError, yesno, False, values='yes')
        self.assertRaises(IndexError, yesno, None, values='yes')
        self.assertRaises(IndexError, yesno, False, values='yes.no')

    def test_status_to_emoji(self):
        """Test status_to_emoji filter/function."""
        self.assertEqual(status_to_emoji('PASS'), '✅')
        self.assertEqual(status_to_emoji('FAIL'), '❌')
        self.assertEqual(status_to_emoji('ERROR'), '⚡')
        self.assertEqual(status_to_emoji('SKIP'), '❔')
        self.assertEqual(status_to_emoji('DONE'), '❔')
        self.assertEqual(status_to_emoji('SKIPPED'), '⏭️')

    def test_join_addresses(self):
        """Test the join_addresses function."""

        data = ['foo', '  bar', 'baz ']
        expected = 'foo, bar, baz'

        self.assertEqual(join_addresses(data), expected)

    def test_unique_test_descriptions(self):
        """Test the unique_test_descriptions filter/function."""
        unique_tests = [MagicMock(architecture='x86_64', debug=False, comment='abc'),
                        MagicMock(architecture='x86_64', debug=False, comment='foo'),
                        MagicMock(architecture='x86_64', debug=True, comment='foo'),
                        MagicMock(architecture='aarch64', debug=False, comment='foo')]

        non_unique_tests = unique_tests.copy()
        non_unique_tests += [MagicMock(architecture='x86_64', debug=False, comment='abc'),
                             MagicMock(architecture='x86_64', debug=False, comment='foo')]

        self.assertEqual(unique_test_descriptions(unique_tests), unique_tests)
        self.assertEqual(unique_test_descriptions(non_unique_tests), unique_tests)
