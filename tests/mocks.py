"""Test mocks."""
from unittest.mock import MagicMock


def get_mocked_checkout(valid=True, builds=None, issues=None, misc=None):
    """Return a kcidb checkout mock."""
    if builds is None:
        builds = []
    if issues is None:
        issues = []
    if misc is None:
        misc = {}
    checkout = MagicMock()
    checkout.valid = valid
    checkout.issues_occurrences.list.return_value = issues
    checkout.misc = misc

    checkout.builds.list.return_value = builds
    return checkout


def get_mocked_build(valid=True, tests=None, issues=None, misc=None):
    """Return a kcidb build mock."""
    if tests is None:
        tests = []
    if issues is None:
        issues = []
    if misc is None:
        misc = {}
    build = MagicMock()
    build.valid = valid
    build.issues_occurrences.list.return_value = issues
    build.misc = misc

    build.tests.list.return_value = tests
    return build


def get_mocked_test(status='PASS', waived=False, issues=None):
    """Return a kcidb test mock."""
    if issues is None:
        issues = []
    test = MagicMock()
    test.status = status
    test.waived = waived
    test.issues_occurrences.list.return_value = issues

    return test


def get_mocked_issue_occurrence(is_regression=False, issue_attributes=None):
    """Return a mock of an issue_occurrence."""
    if issue_attributes is None:
        issue_attributes = {}
    issue_occurrence = MagicMock()
    issue_occurrence.is_regression = is_regression

    issue_occurrence.issue = MagicMock()
    for key, value in issue_attributes.items():
        setattr(issue_occurrence.issue, key, value)

    return issue_occurrence
