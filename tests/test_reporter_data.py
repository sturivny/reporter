from unittest import TestCase
from unittest.mock import MagicMock
from unittest.mock import patch

from reporter.data import CheckoutData

from .mocks import get_mocked_build
from .mocks import get_mocked_checkout
from .mocks import get_mocked_issue_occurrence
from .mocks import get_mocked_test


class TestReporterData(TestCase):
    """Tests for reporter/data.py ."""

    @patch('reporter.data.urlopen')
    @patch('reporter.settings.DATAWAREHOUSE.kcidb.checkouts.get')
    def test_checkout_data(self, checkouts_get_mock, urlopen_mock):
        """Test the CheckoutData class and it's properties."""

        tests = [get_mocked_test('PASS', False)]
        builds = [get_mocked_build(True, tests)]
        issues = ['Issue']
        checkout = get_mocked_checkout(True, builds, issues)

        checkout_id = 'redhat:123'
        checkouts_get_mock.return_value = checkout
        checkout_data = CheckoutData(checkout_id)

        checkouts_get_mock.assert_called_with(id=checkout_id)

        self.assertEqual(checkout_data.checkout, checkout)

        merge_log = MagicMock()
        merge_log.read.side_effect = [b'bar']
        urlopen_mock.return_value.__enter__.return_value = merge_log

        self.assertEqual(checkout_data.mergelog,
                         'bar')

        self.assertEqual(checkout_data.issues_occurrences, issues)

        self.assertEqual(checkout_data.build_data.builds, builds)
        self.assertEqual(checkout_data.test_data.tests, tests)

        self.assertTrue(checkout_data.result)

    @patch('reporter.settings.DATAWAREHOUSE.kcidb.checkouts.get')
    def test_checkout_data_failed_build_result(self, checkouts_get_mock):
        """Test the CheckoutData result property."""
        from reporter.data import CheckoutData
        tests = []
        builds = [get_mocked_build(True, tests),
                  get_mocked_build(False, tests),
                  get_mocked_build(False, tests, ['Issue'])]
        checkout = get_mocked_checkout(True, builds)

        checkouts_get_mock.return_value = checkout
        checkout_data = CheckoutData('1234')

        self.assertFalse(checkout_data.result)
        self.assertFalse(checkout_data.summary)

    @patch('reporter.settings.DATAWAREHOUSE.kcidb.checkouts.get')
    def test_checkout_data_known_issue_build_result(self, checkouts_get_mock):
        """Test the CheckoutData result property."""
        from reporter.data import CheckoutData
        tests = []
        builds = [get_mocked_build(True, tests),
                  get_mocked_build(False, tests, ['Issue'])]
        checkout = get_mocked_checkout(True, builds)

        checkouts_get_mock.return_value = checkout
        checkout_data = CheckoutData('1234')

        self.assertTrue(checkout_data.result)
        self.assertTrue(checkout_data.summary)

    @patch('reporter.settings.DATAWAREHOUSE.kcidb.checkouts.get')
    def test_checkout_data_summary(self, checkouts_get_mock):
        """Test the CheckoutData result property."""
        from reporter.data import CheckoutData
        tests = [get_mocked_test('SKIP')]
        builds = [get_mocked_build(True, tests)]
        checkout = get_mocked_checkout(True, builds)

        checkouts_get_mock.return_value = checkout
        checkout_data = CheckoutData('1234')

        self.assertTrue(checkout_data.result)
        self.assertIsNone(checkout_data.test_data.summary)
        self.assertIsNone(checkout_data.summary)

    def test_build_data(self):
        """Test the BuildData class and it's properties."""
        from reporter.data import BuildData

        tests = []
        passed_build = get_mocked_build(True, tests)
        failed_build = get_mocked_build(False, tests)
        known_failure_build = get_mocked_build(False, tests, ['Issue'])

        builds = MagicMock()
        builds.list.return_value = [passed_build, failed_build,
                                    known_failure_build]

        build_data = BuildData(builds)

        self.assertEqual(build_data.passed_builds, [passed_build])
        self.assertEqual(build_data.failed_builds,
                         [failed_build, known_failure_build])
        self.assertEqual(build_data.known_issues_builds,
                         [known_failure_build])
        self.assertEqual(build_data.unknown_issues_builds, [failed_build])

    def test_build_data_get_kernel_variant(self):
        """Test the BuildData get_kernel_variant static method."""
        from reporter.data import BuildData
        build_default = get_mocked_build(misc={'debug': False, 'package_name': 'kernel'})
        build_debug = get_mocked_build(misc={'debug': True, 'package_name': 'kernel'})
        build_64k = get_mocked_build(misc={'debug': False, 'package_name': 'kernel-64k'})
        build_64k_debug = get_mocked_build(misc={'debug': True, 'package_name': 'kernel-64k'})
        build_64k_debug_package = get_mocked_build(misc={'debug': False,
                                                         'package_name': 'kernel-64k-debug'})
        build_64k_debug_both = get_mocked_build(misc={'debug': True,
                                                      'package_name': 'kernel-64k-debug'})

        self.assertEqual(BuildData.get_kernel_variant(build_default), '')
        self.assertEqual(BuildData.get_kernel_variant(build_debug), 'debug')
        self.assertEqual(BuildData.get_kernel_variant(build_64k), '64k')
        self.assertEqual(BuildData.get_kernel_variant(build_64k_debug), '64k-debug')
        self.assertEqual(BuildData.get_kernel_variant(build_64k_debug_package), '64k-debug')
        self.assertEqual(BuildData.get_kernel_variant(build_64k_debug_both), '64k-debug')
        self.assertEqual(BuildData.get_kernel_variant(build_64k, 'kernel-64k'), '')
        self.assertEqual(BuildData.get_kernel_variant(build_64k_debug, 'kernel-64k'), 'debug')

    def test_test_data(self):
        """Test the TestData class and it's properties."""
        from reporter.data import BuildData
        from reporter.data import TestData
        pass_test = get_mocked_test('PASS', False)
        pass_waived_test = get_mocked_test('PASS', True)
        fail_test = get_mocked_test('FAIL', False)
        fail_waived_test = get_mocked_test('FAIL', True)
        error_test = get_mocked_test('ERROR', False)
        skip_test = get_mocked_test('SKIP', False)
        none_test = get_mocked_test(None, False)
        known_issue_occurrence = get_mocked_issue_occurrence(
            issue_attributes={'description': 'failed', 'resolved': False}
        )
        fail_known_test1 = get_mocked_test('FAIL', False, [known_issue_occurrence])
        fail_known_test2 = get_mocked_test('FAIL', False, [known_issue_occurrence])
        regression = get_mocked_test('FAIL', False, [get_mocked_issue_occurrence(
            is_regression=True,
            issue_attributes={'description': 'failed', 'resolved': True}
        )])
        tests = [pass_test, fail_test, pass_waived_test, fail_waived_test,
                 error_test, skip_test, none_test, fail_known_test1, fail_known_test2, regression]

        builds = MagicMock()
        builds.list.return_value = [get_mocked_build(True, tests)]

        build_data = BuildData(builds)
        test_data = TestData(build_data)

        self.assertEqual(test_data.tests, tests)

        self.assertEqual(test_data.failed_tests,
                         [fail_test, fail_known_test1, fail_known_test2, regression])
        self.assertEqual(test_data.passed_tests,
                         [pass_test, pass_waived_test])
        self.assertEqual(test_data.skipped_tests, [skip_test, none_test])
        self.assertEqual(test_data.non_skipped_tests,
                         [test for test in tests
                          if test not in test_data.skipped_tests])
        self.assertEqual(test_data.errored_tests, [error_test])
        self.assertEqual(test_data.waived_tests,
                         [pass_waived_test, fail_waived_test])
        self.assertEqual(test_data.known_issues_tests, [fail_known_test1, fail_known_test2])
        self.assertEqual(test_data.unknown_issues_tests,
                         [fail_test])  # I'm not expecting the waived test here
        self.assertEqual(test_data.regressions, [regression])
        known_issue = fail_known_test1.issues_occurrences[0].issue
        self.assertEqual(test_data.known_issues,
                         {known_issue['ticket_url']: {'issue': known_issue,
                                                      'tests': [fail_known_test1, fail_known_test2]}
                          }
                         )

    def test_test_data_all_skipped_check(self):
        """Test the TestData's all_skipped_check method."""
        from reporter.data import BuildData
        from reporter.data import TestData
        pass_test = get_mocked_test('PASS', False)
        skip_test1 = get_mocked_test('SKIP', False)
        skip_test2 = get_mocked_test('SKIP', False)

        builds_skipped = MagicMock()
        builds_non_skipped = MagicMock()
        builds_skipped.list.return_value = [get_mocked_build(True, [skip_test1])]
        builds_non_skipped.list.return_value = [get_mocked_build(True, [skip_test2, pass_test])]

        build_data_skipped = BuildData(builds_skipped)
        build_data_non_skipped = BuildData(builds_non_skipped)
        test_data_skipped = TestData(build_data_skipped)
        test_data_non_skipped = TestData(build_data_non_skipped)

        self.assertTrue(test_data_skipped.all_skipped_check)
        self.assertFalse(test_data_non_skipped.all_skipped_check)
