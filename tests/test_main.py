from argparse import Namespace
import re
from unittest import TestCase
from unittest.mock import MagicMock
from unittest.mock import patch

from .utils import load_json


class TestMain(TestCase):
    """Tests for reporter/__main__.py ."""

    default_args = Namespace(listen=False, checkout_id=None, template=None, email=False)
    listen_args = Namespace(listen=True, checkout_id=None, template=None, email=False)
    checkout_args = Namespace(listen=False, checkout_id='redhat:123',
                              template=None, email=False)
    template_args = Namespace(listen=False, checkout_id='redhat:123',
                              template='long', email=False)
    email_args = Namespace(listen=False, checkout_id='redhat:123',
                           template=None, email=True)

    @patch('reporter.__main__.CheckoutData')
    def test_process_message(self, data_mock):
        """Test the process_message function."""
        from reporter.__main__ import ReporterException
        from reporter.__main__ import process_message

        processing_func_mock = MagicMock()

        expected_data = {
            'timestamp': '2020-07-28T14:51:09.845612+00:00',
            'status': 'ready_to_report',
            'object_type': 'checkout',
            'id': 'redhat:123'
        }

        process_message(body=expected_data, func=processing_func_mock)

        self.assertEqual(processing_func_mock.call_count, 1)

        # Call with an unexpected object_type
        with patch.dict(expected_data, {'object_type': 'foo'}):
            with self.assertRaises(ReporterException):
                process_message(body=expected_data, func=processing_func_mock)
        self.assertEqual(processing_func_mock.call_count, 1)

        # Call again with a status that should be ignored
        with patch.dict(expected_data, {'status': 'new'}):
            process_message(body=expected_data, func=processing_func_mock)
        self.assertEqual(processing_func_mock.call_count, 1)

        with patch.dict(expected_data, {'object': {'misc': {'retrigger': True}}}):
            process_message(body=expected_data, func=processing_func_mock)
        self.assertEqual(processing_func_mock.call_count, 1)

        with patch.dict(expected_data, {'object': {'misc': {'related_merge_request': True}}}):
            process_message(body=expected_data, func=processing_func_mock)
        self.assertEqual(processing_func_mock.call_count, 1)

        # test that none of the previous patches had any effect permanent effect
        process_message(body=expected_data, func=processing_func_mock)
        self.assertEqual(processing_func_mock.call_count, 2)

    def test_create_parser(self):
        """Test the create_parser function."""
        from reporter.__main__ import create_parser

        parser = create_parser()

        args = parser.parse_args([])
        self.assertEqual(args, self.default_args)

        args = parser.parse_args(['-l'])
        self.assertEqual(args, self.listen_args)

        args = parser.parse_args(['-c', self.checkout_args.checkout_id])
        self.assertEqual(args, self.checkout_args)

        with self.assertRaises(SystemExit):
            parser.parse_args(['-l', '-c', self.checkout_args.checkout_id])

    @patch('cki_lib.misc.is_production')
    @patch('cki_lib.misc.sentry_init')
    @patch('argparse.ArgumentParser.parse_args')
    @patch('reporter.settings.QUEUE.consume_messages')
    def test_main_default(self, consume_messages_mock, args_mock,
                          sentry_init_mock, is_production_mock):
        """Test main with no arguments."""
        from reporter.__main__ import main

        args_mock.return_value = self.default_args
        is_production_mock.return_value = True
        main()
        assert consume_messages_mock.called
        assert sentry_init_mock.called

    @patch('argparse.ArgumentParser.parse_args')
    @patch('reporter.settings.QUEUE.consume_messages')
    def test_main_listen(self, consume_messages_mock, args_mock):
        """Test main with listen argument."""
        from reporter.__main__ import main

        args_mock.return_value = self.listen_args
        main()
        assert consume_messages_mock.called

    @patch('argparse.ArgumentParser.parse_args')
    @patch('reporter.__main__.CheckoutData')
    def test_main_checkout_id(self, checkout_data_mock, args_mock):
        """Test main with checkout id specified."""
        from reporter.__main__ import main

        args_mock.return_value = self.checkout_args
        with self.assertRaises(SystemExit):
            main()
        checkout_data_mock.assert_called_once_with(
            self.checkout_args.checkout_id)

    @patch('reporter.__main__.send_emails', MagicMock())
    @patch('argparse.ArgumentParser.parse_args')
    @patch('reporter.__main__.CheckoutData')
    def test_main_template(self, checkout_data_mock, args_mock):
        """Test main with checkout id specified."""
        from reporter import settings
        from reporter.__main__ import main

        args_mock.return_value = self.template_args
        with self.assertRaises(SystemExit):
            main()
        checkout_data_mock.assert_called_once_with(
            self.template_args.checkout_id)

        self.assertEqual(settings.SELECTED_TEMPLATE, self.template_args.template)

    @patch('reporter.__main__.send_emails', MagicMock())
    @patch('argparse.ArgumentParser.parse_args')
    @patch('reporter.__main__.CheckoutData')
    def test_main_exit(self, checkout_data_mock, args_mock):
        """Test main with checkout id specified."""
        from reporter.__main__ import main

        args_mock.return_value = self.template_args

        checkout_data_mock.return_value.result = True
        with self.assertRaises(SystemExit) as exit_exception:
            main()
        self.assertEqual(exit_exception.exception.code, 0)

        checkout_data_mock.return_value.result = False
        with self.assertRaises(SystemExit) as exit_exception:
            main()
        self.assertEqual(exit_exception.exception.code, 1)

    @patch('argparse.ArgumentParser.parse_args')
    @patch('builtins.print')
    @patch('reporter.__main__.render_template')
    @patch('reporter.__main__.CheckoutData')
    def test_main_print(self, checkout_data_mock, render_template_mock, print_mock, args_mock):
        """Test main with listen argument."""
        from reporter.__main__ import main

        args_mock.return_value = self.checkout_args
        with self.assertRaises(SystemExit):
            main()
        checkout_data_mock.assert_called_once_with(self.checkout_args.checkout_id)
        render_template_mock.assert_called_once_with(checkout_data_mock())
        print_mock.assert_called_once_with(render_template_mock())

    @patch('reporter.emailer.get_subject', MagicMock(return_value="subject"))
    @patch('smtplib.SMTP', MagicMock())
    @patch('argparse.ArgumentParser.parse_args')
    @patch('reporter.emailer.send_emails')
    @patch('reporter.__main__.CheckoutData')
    def test_main_send_emails(self, checkout_data_mock, send_emails_mock, args_mock):
        """Test main with listen argument."""
        from reporter.__main__ import main

        args_mock.return_value = self.email_args
        with self.assertRaises(SystemExit):
            main()
        checkout_data_mock.assert_called_once_with(self.email_args.checkout_id)
        send_emails_mock.called_once_with(checkout_data_mock())

    @patch('reporter.__main__.CheckoutData')
    def test_render_template_shows_status_and_emojis(self, mock):
        """Test that the report shows the test statuses and their emojis."""
        from reporter.settings import JINJA_ENV
        test_data = load_json('test_data.json')
        mock.test_data.configure_mock(**test_data)
        template = JINJA_ENV.get_template('partials/all_tests.j2')
        text = template.render({'test_data': mock.test_data}).strip()
        # Keep only the part of the template that we're testing.
        m = re.match(r'(.*)Aborted tests', text, re.DOTALL)
        text = m.group(1)
        self.assertIn('✅ PASS LTP - syscalls', text)
        self.assertIn('❌ FAIL LTP - syscalls', text)
        self.assertIn('⚡ 🚧 ERROR kdump - sysrq-c', text)

    @patch('reporter.__main__.CheckoutData')
    def test_render_template_shows_subtests(self, mock):
        """Test that the report shows the non-passing subtests."""
        from reporter.settings import JINJA_ENV
        test_data = load_json('test_data.json')
        mock.test_data.configure_mock(**test_data)
        template = JINJA_ENV.get_template('partials/all_tests.j2')

        text = template.render({'test_data': mock.test_data}).strip()

        self.assertIn('❌ FAIL subtest1', text)
        self.assertIn('Subtests skipped: 1 out of 3', text)
        self.assertNotIn('subtest2', text)
        self.assertNotIn('subtest3', text)
        self.assertNotIn('subtest4', text)

    @patch('reporter.__main__.CheckoutData')
    def test_render_template_build_debug(self, mock):
        """Test that the report works for all build debug values and exceptions."""
        from reporter.settings import JINJA_ENV
        build_data = load_json('build_data.json')
        mock.build_data.configure_mock(**build_data)
        template = JINJA_ENV.get_template('partials/all_builds.j2')
        print(build_data)
        print(mock.build_data)
        text = template.render({'build_data': mock.build_data}).strip()

        self.assertIn('x86_64 (debug)', text)
        self.assertIn('ppc64', text)
        self.assertNotIn('ppc64 (debug)', text)
        self.assertIn('aarch64', text)
        self.assertNotIn('aarch64 (debug)', text)
        self.assertIn('aarch64 (64k)', text)
        self.assertIn('aarch64 (64k-debug)', text)
        self.assertNotIn('aarch64 ()', text)
