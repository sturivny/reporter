from email.message import EmailMessage
from importlib import reload
import os
from unittest import TestCase
from unittest.mock import MagicMock
from unittest.mock import patch

from reporter import emailer
from reporter import settings
from reporter.utils import join_addresses

from .utils import load_json


class TestEmailer(TestCase):
    """Tests for reporter/emailer.py ."""

    @patch.dict(os.environ, {'REPORTER_EMAIL_FROM': 'foo@bar.baz',
                             'BCC_ARCHIVE_ADDRESS': 'archive@bar.baz'})
    @patch('reporter.emailer.get_subject')
    def test_build_email_headers(self, get_subject_mock):
        """Test the build_email function."""
        reload(settings)

        checkout_data_mock = MagicMock()
        get_subject_mock.return_value = 'SUBJECT'

        recipients = load_json('recipients.json')['test1']

        checkout_data_mock = MagicMock()

        msg = emailer.build_email_headers(checkout_data_mock, recipients)
        get_subject_mock.assert_called_once_with(checkout_data_mock)
        self.assertEqual(msg['Subject'], 'SUBJECT')
        self.assertEqual(msg['From'], settings.REPORTER_EMAIL_FROM)
        self.assertEqual(msg['To'], 'foo, bar')
        self.assertEqual(msg['BCC'], join_addresses(['fubar', settings.BCC_ARCHIVE_ADDRESS]))

    @patch.dict(os.environ, {'REPORTER_EMAIL_FROM': 'foo@bar.baz',
                             'BCC_ARCHIVE_ADDRESS': 'archive@bar.baz'})
    @patch('reporter.emailer.get_subject')
    def test_build_email_headers_no_bcc(self, get_subject_mock):
        """
        Test the build_email function when there were no BCC
        prior to adding the archive address.
        """
        reload(settings)

        checkout_data_mock = MagicMock()
        get_subject_mock.return_value = 'SUBJECT'

        recipients = load_json('recipients.json')['test3']

        checkout_data_mock = MagicMock()

        msg = emailer.build_email_headers(checkout_data_mock, recipients)
        get_subject_mock.assert_called_once_with(checkout_data_mock)
        self.assertEqual(msg['Subject'], 'SUBJECT')
        self.assertEqual(msg['From'], settings.REPORTER_EMAIL_FROM)
        self.assertEqual(msg['To'], 'foo, bar')
        self.assertEqual(msg['BCC'], settings.BCC_ARCHIVE_ADDRESS)

    @patch.dict(os.environ, {'SMTP_URL': 'test'})
    @patch('cki_lib.misc.is_production', MagicMock(return_value=True))
    @patch('smtplib.SMTP')
    def test_send_emails(self, mock_smtp):
        """Ensure we can send email messages properly."""
        reload(settings)
        reload(emailer)
        settings.EMAIL_SENDING = True
        mock_smtp.return_value.send_message.return_value = None

        recipients_mock = MagicMock()
        recipients_mock.attributes = load_json('grouped_recipients.json')

        checkout_data_mock = MagicMock()
        recipients_get = checkout_data_mock.checkout.recipients.get
        recipients_get.return_value = recipients_mock

        with patch('reporter.emailer.get_subject', MagicMock(return_value="subject")):
            emailer.send_emails(checkout_data_mock)

        self.assertEqual(mock_smtp.call_args[0][0], 'test')
        self.assertEqual(
            'a@a.com, b@b.com, d@d.com',
            mock_smtp.return_value.send_message.call_args[0][0]['To']
        )
        mock_smtp.return_value.set_debuglevel.assert_not_called()

        os.environ['SMTP_DEBUG'] = 'True'
        reload(settings)
        reload(emailer)
        settings.EMAIL_SENDING = True

        with patch('reporter.emailer.get_subject', MagicMock(return_value="subject")):
            emailer.send_emails(checkout_data_mock)

        self.assertEqual(mock_smtp.call_args[0][0], 'test')
        mock_smtp.return_value.set_debuglevel.assert_called_once_with(2)

    def test_set_default_template(self):
        """Test the set_default_template function."""
        settings.SELECTED_TEMPLATE = 'short'
        recipients = load_json('grouped_recipients.json')

        emailer.set_default_template(recipients)

        self.assertEqual(
            ['a@a.com', 'b@b.com', 'd@d.com'],
            recipients['short']['To']
        )
        self.assertEqual(
            ['c@c.com'],
            recipients['short']['BCC']
        )

        settings.SELECTED_TEMPLATE = 'long'
        recipients = load_json('grouped_recipients.json')

        emailer.set_default_template(recipients)

        self.assertEqual(
            ['d@d.com'],
            recipients['long']['To']
        )

    @patch.dict(os.environ, {'SMTP_URL': 'test'})
    @patch('cki_lib.misc.is_production', MagicMock(return_value=False))
    @patch('reporter.settings.LOGGER.info')
    @patch('smtplib.SMTP')
    def test_send_emails_non_production(self, mock_smtp, mock_info):
        """Ensure we only log the email message when not in production."""
        reload(settings)
        reload(emailer)
        settings.EMAIL_SENDING = True

        recipients_mock = MagicMock()
        recipients_mock.attributes = load_json('grouped_recipients.json')

        checkout_data_mock = MagicMock()
        recipients_get = checkout_data_mock.checkout.recipients.get
        recipients_get.return_value = recipients_mock

        with patch('reporter.emailer.get_subject', MagicMock(return_value="subject")):
            emailer.send_emails(checkout_data_mock)

        mock_smtp.return_value.send_message.assert_not_called()
        mock_info.assert_called_once()
        self.assertTrue(isinstance(mock_info.call_args.args[0], EmailMessage))

    def test_get_subject(self):
        """Test if get_subject works properly in all common cases."""
        checkout_data_mock = MagicMock()

        checkout_data_mock.checkout.misc = {}
        checkout_data_mock.checkout.patchset_hash = '123456789'
        checkout_data_mock.checkout.git_repository_branch = None
        checkout_data_mock.checkout.tree_name = None

        checkout_data_mock.test_data.tests = [MagicMock(), MagicMock()]
        checkout_data_mock.summary = None

        subject = emailer.get_subject(checkout_data_mock)
        self.assertIn('SKIPPED', subject)
        self.assertIn('12345678', subject)
        self.assertNotIn('SKIPPED 0 of 1', subject)
        self.assertNotIn('123456789', subject)

        checkout_data_mock.summary = True

        subject = emailer.get_subject(checkout_data_mock)
        self.assertIn('PASS', subject)
        self.assertIn('12345678', subject)
        self.assertNotIn('SKIPPED 0 of 2', subject)
        self.assertNotIn('123456789', subject)

        checkout_data_mock.test_data.skipped_tests = [MagicMock()]

        subject = emailer.get_subject(checkout_data_mock)
        self.assertIn('PASS', subject)
        self.assertIn('12345678', subject)
        self.assertIn('SKIPPED 1 of 2', subject)
        self.assertNotIn('123456789', subject)

        checkout_data_mock.checkout.misc = {'kernel_version': '1.2.3'}
        subject = emailer.get_subject(checkout_data_mock)
        self.assertIn('1.2.3', subject)
        self.assertIn('(12345678)', subject)

        checkout_data_mock.summary = False
        checkout_data_mock.checkout.patchset_hash = None
        checkout_data_mock.checkout.git_commit_hash = '987654321'
        subject = emailer.get_subject(checkout_data_mock)
        self.assertIn('FAIL', subject)
        self.assertIn('1.2.3', subject)
        self.assertIn('(98765432)', subject)

        checkout_data_mock.checkout.git_commit_hash = None
        subject = emailer.get_subject(checkout_data_mock)
        self.assertIn('FAIL', subject)
        self.assertIn('1.2.3', subject)
        self.assertNotIn('(98765432)', subject)

        checkout_data_mock.checkout.patchset_hash = '123456789'
        checkout_data_mock.checkout.git_repository_branch = 'foobar'
        checkout_data_mock.checkout.tree_name = 'baz'
        subject = emailer.get_subject(checkout_data_mock)
        self.assertIn('foobar (1.2.3, baz, 12345678)', subject)
