"""Data format to be passed to the template."""
from urllib.request import urlopen

from cached_property import cached_property

from . import settings


class TestData:
    """Data about all checkout's tests."""

    def __init__(self, build_data):
        """Initialize."""
        tests = []
        for build in build_data.builds:
            build_tests = build.tests.list()
            for test in build_tests:
                # Overwriting test.issues_occurrences because after getting a
                # list of all of them, we shouldn't need them anymore
                test.issues_occurrences = test.issues_occurrences.list()
                test.architecture = build.architecture
                test.variant = build.variant
                test.debug = build.misc['debug'] if 'debug' in build.misc else False
                test.waived_failure = test.status == 'FAIL' and test.waived
                test.subtests = test.results.list()
                # Filtering subtests so we don't have to do it in jinja
                test.skipped_subtests = [subtest for subtest in test.subtests
                                         if subtest.status == 'SKIP']
                test.non_passing_subtests = [subtest for subtest in test.subtests
                                             if subtest.status != 'PASS' and
                                             subtest not in test.skipped_subtests]

            tests += build_tests

        self.tests = tests

    def _tests_filtered(self, condition, exclude_waived=False):
        tests = self.tests
        if exclude_waived:
            tests = filter(lambda x: not x.waived, tests)
        return list(filter(condition, tests))

    @cached_property
    def non_skipped_tests(self):
        """Get a list of build's non-skipped tests."""
        return self._tests_filtered(lambda test:
                                    test not in self.skipped_tests)

    @cached_property
    def passed_tests(self):
        """Get a list of build's tests that passed."""
        return self._tests_filtered(lambda test: test.status == 'PASS')

    @cached_property
    def skipped_tests(self):
        """Get a list of build's tests that got skipped."""
        return self._tests_filtered(lambda test: test.status is None or
                                    test.status == 'SKIP')

    @cached_property
    def failed_tests(self):
        """Get a list of build's tests that failed."""
        return self._tests_filtered(lambda test: test.status == 'FAIL',
                                    exclude_waived=True)

    @cached_property
    def errored_tests(self):
        """Get a list of build's tests that encountered an infra error."""
        return self._tests_filtered(lambda test: test.status == 'ERROR')

    @cached_property
    def waived_tests(self):
        """Get a list of waived build's tests."""
        return self._tests_filtered(lambda test: test.waived)

    @cached_property
    def known_issues_tests(self):
        """Get a list of build's tests that failed due to known issues."""
        return self._tests_filtered(lambda test: test.issues_occurrences and
                                    test not in self.regressions)

    @cached_property
    def unknown_issues_tests(self):
        """Get a list of build's tests that failed due to unknown issues."""
        return self._tests_filtered(lambda test: test.status == 'FAIL' and
                                    test not in self.known_issues_tests and
                                    test not in self.waived_tests and
                                    test not in self.regressions)

    @cached_property
    def regressions(self):
        """Get a list of tests that have resolved issues."""
        return self._tests_filtered(
            lambda test: any(occurrence.is_regression for occurrence in
                             test.issues_occurrences),
            exclude_waived=True
        )

    @cached_property
    def known_issues(self):
        """Get a list of all issues and the tests they affect."""
        issues = {}  # {"issue_ticket_url": {"issue": issue, "tests": [test1, test2, ...], ...}}
        for test in self.known_issues_tests:
            for occurrence in test.issues_occurrences or []:
                if occurrence.issue['ticket_url'] not in issues:
                    issues[occurrence.issue['ticket_url']] = {'issue': occurrence.issue,
                                                              'tests': []}
                issues[occurrence.issue['ticket_url']]['tests'].append(test)

        return issues

    @cached_property
    def all_skipped_check(self):
        """
        Check whether all tests were skipped.

        True if every test was skipped, False otherwise.
        """
        return not self.non_skipped_tests

    @cached_property
    def summary(self):
        """
        Get the test_data summary.

        None if all tests were skipped, False if unknown failures found, True otherwise.
        """
        if self.tests and self.all_skipped_check:
            return None
        return not self.unknown_issues_tests


class BuildData:
    """Data about all checkout's builds."""

    def __init__(self, builds, source_package_name='kernel'):
        """Initialize."""
        self.builds = builds.list()
        for build in self.builds:
            # Overwriting build.issues_occurrences because after getting a list
            # of all of them, we shouldn't need them anymore
            build.issues_occurrences = build.issues_occurrences.list()
            if build.log:
                build.log_url = build.log.url
            # Rename in case this starts conflicting with an attribute
            build.variant = self.get_kernel_variant(build, source_package_name)

    @cached_property
    def passed_builds(self):
        """Return passing builds."""
        return list(filter(lambda x: x.valid is True, self.builds))

    @cached_property
    def failed_builds(self):
        """Return failing builds."""
        return list(filter(lambda x: x.valid is False, self.builds))

    @cached_property
    def known_issues_builds(self):
        """Get a list of builds that failed due to known issues."""
        return list(filter(lambda x: x.issues_occurrences, self.builds))

    @cached_property
    def unknown_issues_builds(self):
        """Get a list of builds that failed due to unknown issues."""
        return list(filter(lambda x: not x.issues_occurrences, self.failed_builds))

    @staticmethod
    def get_kernel_variant(build, source_package_name='kernel'):
        """Return builds "variant" to include with its architecture."""
        modifiers = []
        package_name = build.misc.get('package_name', 'kernel')
        debug = build.misc.get('debug', False)
        if package_name != source_package_name:
            modifiers.append(package_name.removeprefix(source_package_name).removeprefix('-'))
        if debug and 'debug' not in package_name:
            modifiers.append('debug')
        return '-'.join(modifiers)


class CheckoutData:
    """Data about a single checkout."""

    def __init__(self, checkout_id):
        """initialize."""
        checkouts = settings.DATAWAREHOUSE.kcidb.checkouts
        self.checkout = checkouts.get(id=checkout_id)

    @cached_property
    def build_data(self):
        """Get the BuildData object which contains information about builds."""
        return BuildData(self.checkout.builds,
                         self.checkout.misc.get('source_package_name', 'kernel'))

    @cached_property
    def test_data(self):
        """Get the TestData object which contains information about tests."""
        return TestData(self.build_data)

    @cached_property
    def issues_occurrences(self):
        """Return the checkouts's known issues."""
        return self.checkout.issues_occurrences.list()

    @cached_property
    def mergelog(self):
        """Return the checkout's mergelog file."""
        with urlopen(self.checkout.log_url) as log:
            return log.read().decode('utf-8')

    @cached_property
    def result(self):
        """
        Get the overall result.

        True if everything passed, False otherwise.
        """
        return (self.checkout.valid is not False and  # Don't fail if valid is None
                not self.build_data.unknown_issues_builds and
                not self.test_data.unknown_issues_tests and
                not self.test_data.regressions)

    @cached_property
    def summary(self):
        """
        Get the checkout summary, same as the result except when all tests were skipped.

        None if all tests were skipped, True if everything passed, False otherwise.
        """
        if self.test_data.summary is None:
            return None
        return self.result
